const themeBtn = document.querySelector(".theme");
const heading = document.querySelector(".registration-form-item");

document.documentElement.style.setProperty(
  "#ffffff",
  localStorage.getItem("register-list")
);

heading.style.backgroundColor = localStorage.getItem("navBarColor");

themeBtn.addEventListener("click", (e) => {
  if (localStorage.getItem("themeColor") === "white") {
    document.documentElement.style.setProperty("#ffffff", "black");
    heading.style.backgroundColor = "rgba(60, 184, 120, 0.8)";
    localStorage.setItem("themeColor", "black");
    localStorage.setItem("register-list", "#000000");
    localStorage.setItem("navBarColor", "rgba(60, 184, 120, 0.8)");
  } else {
    document.documentElement.style.setProperty(
      "--primary-text-color",
      "#ffffff"
    );
    heading.style.backgroundColor = "#ab1f09";
    localStorage.setItem("themeColor", "white");
    localStorage.setItem("register-list", "#ffffff");
    localStorage.setItem("navBarColor", "#ab1f09");
  }
});
