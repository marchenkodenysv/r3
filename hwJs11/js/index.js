document.addEventListener("DOMContentLoaded", function (event) {
  document.querySelectorAll(".fas").forEach((icon) => {
    icon.addEventListener("click", () => {
      if (icon.previousElementSibling.type === "password") {
        icon.previousElementSibling.type = "text";
        icon.classList.remove("fa-eye");
        icon.classList.add("fa-eye-slash");
      } else {
        icon.previousElementSibling.type = "password";
        icon.classList.remove("fa-eye-slash");
        icon.classList.add("fa-eye");
      }
    });
  });
});

const btn = document.querySelector(".btn");
btn.addEventListener("click", (e) => {
  e.preventDefault();
  const inputs = document.querySelectorAll("input");
  const error = document.querySelector("#error");
  if (
    inputs[0].value === inputs[1].value &&
    inputs[0].value !== "" &&
    inputs[1].value !== ""
  ) {
    if (document.body.contains(error)) {
      error.remove();
    }
    alert("You are welcome");
  } else {
    if (!document.body.contains(error)) {
      inputs[1].insertAdjacentHTML(
        "afterend",
        "<span id='error' style='color: red'>Нужно ввести одинаковые значения</span>"
      );
    }
  }
});
