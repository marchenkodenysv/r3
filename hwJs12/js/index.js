const color = document.querySelectorAll(".btn");

document.addEventListener("keydown", (e) => {
  color.forEach((element) => {
    element.style.backgroundColor = "#000000";
    if (
      element.textContent.toUpperCase() === e.key.toUpperCase() ||
      `key${element.textContent}` === e.code
    ) {
      element.style.backgroundColor = "blue";
    }
  });
});

// Чому для роботи з input не рекомендується використовувати клавіатуру?
// событие input не происходит при вводе с клавиатуры или иных действиях,
//  если при этом не меняется значение в текстовом поле,
//   т.е. нажатия клавиш ⇦, ⇨ и подобных при фокусе на текстовом поле
//    не вызовут это событие.
